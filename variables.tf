variable "common_name" {
  default = "localhost"
}

variable "dns_names" {
  default = [
    "localhost",
  ]
}

variable "ip_addresses" {
  default = [
    "127.0.0.1",
  ]
}

variable "pki_backend_path" {
  default = "pki"
}

variable "pki_role_name" {
  default = "server"
}

variable "address" {}
variable "token" {}
variable "ca_cert_file" {
  default = ""
}
