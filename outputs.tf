output "private_key_pem" {
  value     = tls_private_key.server.private_key_pem
  sensitive = true
}

output "certificate_pem" {
  value     = vault_pki_secret_backend_sign.server.certificate
  sensitive = true
}

output "ca_chain_pem" {
  value     = vault_pki_secret_backend_sign.server.ca_chain
  sensitive = true
}
