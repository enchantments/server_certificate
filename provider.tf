provider "vault" {
  address      = var.address
  token        = var.token
  ca_cert_file = var.ca_cert_file
}
