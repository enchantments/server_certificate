resource "vault_pki_secret_backend_sign" "server" {

  backend = var.pki_backend_path
  name    = var.pki_role_name

  csr         = tls_cert_request.server.cert_request_pem
  common_name = tls_cert_request.server.subject[0].common_name
}

resource "tls_private_key" "server" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "server" {
  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.server.private_key_pem

  subject {
    common_name = var.common_name
  }

  dns_names = var.dns_names

  ip_addresses = var.ip_addresses
}
