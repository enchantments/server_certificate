# Server Certificate

Creates a TLS certificate for a host based on DNS or IP subject alternative names and Vault connection information.
